$in = $args[0]
$out = $args[1]
$crf = $args[2]
$cu = $args[3]
$grain = $args[4]
$grainBlock = $args[5]

#-vf "scale=-2:1080"

ffmpeg -i $in -c:v libsvtav1 -b:v 0 -crf $crf -pass 2 -passlogfile "$out.$crf.av1.passlog" -g 300 -preset $cu -pix_fmt yuv420p10le -svtav1-params "film-grain=$grain" -c:a libopus ".\$out\$out.$cu.$crf.av1svt.webm"