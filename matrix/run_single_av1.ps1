$in = $args[0]
$out = $args[1]
$crf = $args[2]
$cu = $args[3]
$grain = $args[4]
$grainBlock = $args[5]

#-vf "scale=-2:1080"

ffmpeg -i $in -c:v libaom-av1 -b:v 0 -crf $crf -pass 2 -passlogfile "$out.$crf.av1.passlog" -cpu-used $cu -tiles 1x2 -lag-in-frames 35 -pix_fmt yuv420p10le -denoise-noise-level $grain -denoise-block-size $grainBlock -aom-params "enable-dnl-denoising=0" -c:a libopus ".\$out\$out.$cu.$crf.av1.webm"