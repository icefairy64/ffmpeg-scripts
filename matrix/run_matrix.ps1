[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [string] $Source,

    [Parameter(Mandatory = $true)]
    [string] $Target,

    [Parameter(Mandatory = $true)]
    [string] $Format,

    [int] $CrfFrom = 6, 
    [int] $CrfTo = 14,
    [int] $CrfInterval = 2,

    [int] $CuFrom = 6, 
    [int] $CuTo = 6,
    [int] $CuInterval = 1,

    [int] $GrainLevel = 0,
    [int] $GrainBlockSize = 32,

    [int] $Jobs = 5,
    [int] $Duration = 5
)

if (($Format -eq "vp9") -and ($CuFrom -gt 4)) {
    $CuFrom = 4
    $CuTo = 4
}

$len = $Duration

if (-not (Test-Path "${len}s_$Source")) {
    ffmpeg -t "${len}s" -i $Source -c:v copy -c:a copy "${len}s_$Source"
}

if (-not (Test-Path "$Target")) {
    New-Item "$Target" -ItemType "Directory"
}

for ($crf = $CrfFrom; $crf -le $CrfTo; $crf += $CrfInterval) {
    Write-Output "Pass 1 CRF$crf at CU $cu"

    if ($Format -eq "av1") {
        ffmpeg -i "${len}s_$Source" -c:v libaom-av1 -b:v 0 -crf $crf -pass 1 -passlogfile "$Target.$crf.av1.passlog" -tiles 1x2 -cpu-used 6 -pix_fmt yuv420p10le -denoise-noise-level $GrainLevel -denoise-block-size $GrainBlockSize -aom-params "enable-dnl-denoising=0" -an -f null NUL 2>&1 | Out-Null
    } elseif ($Format -eq "av1svt") {
        ffmpeg -i "${len}s_$Source" -c:v libsvtav1 -b:v 0 -crf $crf -pass 1 -passlogfile "$Target.$crf.av1.passlog" -pix_fmt yuv420p10le -an -f null NUL 2>&1 | Out-Null
    } elseif ($Format -eq "vp9") {
        ffmpeg -i "${len}s_$Source" -c:v libvpx-vp9 -b:v 0 -crf $crf -pass 1 -passlogfile "$Target.$crf.passlog" -speed 4 -pix_fmt yuv420p10le -profile:v 2 -an -f null NUL 2>&1 | Out-Null
    }

    for ($cu = $CuFrom; $cu -le $CuTo; $cu += $CuInterval) {
        $running = @(Get-Job | Where-Object { $_.State -eq 'Running' })
        if ($running.Count -ge $Jobs) {
            $running | Wait-Job -Any | Out-Null
        }

        Write-Output "Transcoding CRF$crf at CU $cu"

        $dir = Get-Location
        Start-Job {
            $time = Measure-Command {
                Set-Location $using:dir | Out-Null
                if ($using:Format -eq "av1") {
                    .\run_single_av1.ps1 "$($using:len)s_$using:Source" $using:Target $using:crf $using:cu $using:GrainLevel $using:GrainBlockSize 2>&1 | Out-Null
                } elseif ($using:Format -eq "av1svt") {
                    .\run_single_av1sv.ps1 "$($using:len)s_$using:Source" $using:Target $using:crf $using:cu $using:GrainLevel 2>&1 | Out-Null
                } elseif ($using:Format -eq "vp9") {
                    .\run_single_vp9.ps1 "$($using:len)s_$using:Source" $using:Target $using:crf $using:cu 2>&1 | Out-Null
                }
            }
            Write-Output "CRF$using:crf CU$using:cu : $($time.TotalSeconds)s"
        } | Out-Null
    }
    if ($crf -gt 24) {
        $crf += 2
    }
}

Wait-Job * | Out-Null
Receive-Job * > "$Target/times.txt"
Remove-Job -State Completed

Remove-Item "*.passlo*"