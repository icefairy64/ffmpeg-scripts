$in = $args[0]
$out = $args[1]
$crf = $args[2]
$cu = $args[3]

ffmpeg -i $in -c:v libvpx-vp9 -b:v 0 -crf $crf -row-mt 1 -pass 2 -passlogfile "$out.$crf.passlog" -cpu-used $cu -tile-columns 2 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 -pix_fmt yuv420p10le -profile:v 2 -c:a libopus ".\$out\$out.$cu.$crf.webm"